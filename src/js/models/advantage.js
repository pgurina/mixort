export default [
    {
        title: "Experts",
        iconClass: "experts",
        text: "In latest technologies. Over 80 full-time employees"
    },
    {
        title: "Know-how",
        iconClass: "know-how",
        text: "We sucessfully deliver pojects since 2008"
    },
    {
        title: "Full cycle",
        iconClass: "full-cycle",
        text: "We guarantee your projects are ready for launch"
    },
    {
        title: "Warranty",
        iconClass: "warranty",
        text: "We make sure your projects are ready for launch"
    },
    {
        title: "Communication",
        iconClass: "communication",
        text: "We always stay in touch via any preferable communication channel"
    },
    {
        title: "Flexible&nbsp;payments",
        iconClass: "payments",
        text: "Choose payment plans and means that is convinient for you"
    }
]
