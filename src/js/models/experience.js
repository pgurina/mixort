export default [
    {
        title: "Business solutions",
        link: "",
        list: [
            "ERP and CRM systems",
            "Corporate portals",
            "Data analytics and visualization",
            "Integration"
        ]
    },
    {
        title: "E-commerce",
        link: "",
        list: [
            "Beautiful web stores",
            "Backoffice integration",
            "Warehouse automation",
            'Integration <span class="m-nowrap">with payment</span> <span class="m-nowrap">and shipping systems</span>'
        ]
    },
    {
        title: "Blockchain",
        link: "",
        list: [
            "Decentralized web applications",
            "Corporate polls and voting",
            "ICO and Token emmission",
            "Decentralized platforms"
        ]
    },
    {
        title: 'Booking and <span class="m-nowrap">event platforms</span>',
        link: "",
        list: [
            "Booking platforms",
            "Website from hotel website",
            "PMS integration",
            "Custom PMS solutions"
        ]
    },
    {
        title: "FinTech",
        link: "",
        list: [
            "Banking applications",
            "Online payment processing",
            "Digital assets management",
            "Trading portals"
        ]
    }
]