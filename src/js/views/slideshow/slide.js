import React from 'react'

const SlideView = ({...props}) => {
    return <div className="m-slide">
        <div className="row">
            {props.children}
        </div>
    </div>
}

export default SlideView