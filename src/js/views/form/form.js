import React from 'react'

const FormView = ({...props})=>{
    return (
        <form className="m-form">
            {props.children}
        </form>
    )
}

export default FormView