import React from 'react'

const CardMenuView = ({...props}) => (<ul className="m-card__menu m-card__menu--right nav">{props.children}</ul>)

export default CardMenuView