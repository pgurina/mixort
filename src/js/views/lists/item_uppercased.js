import React from 'react'

const ItemUppercasedView = ({...props}) => (
    <li className="m-list__item">
        <a href={props.itemID} className={`m-list__link m-list__link--uppercased ${props.activeItemID===props.itemID ? 'active' : ''}`}>{props.children}</a>
    </li>
)

export default ItemUppercasedView