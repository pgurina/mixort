import React from 'react'

const BrickView = ({...props}) => {
    const content = !!props.content ? <img src={props.content}/> : ''

    return <a href="#" className={`m-brick ${props.className}`}>
        <span className="m-brick__content" style={!!props.image ? {backgroundImage: `url(${props.image})`} : {}}>
            <span className="m-brick__text">
                {content}
            </span>
        </span>
        <span className="m-brick__overlay">
            <span className="m-brick__text">
                <img src={props.overlayContent}/>
            </span>
        </span>
    </a>
}

export default BrickView