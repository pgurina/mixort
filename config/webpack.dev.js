const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');

module.exports = merge(common,{
  devtool: "inline-source-map",
  devServer: {
    contentBase: './app'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [{
            loader: "css-loader",
            options: {
                minimize: true
            }
          },  {
            loader: "sass-loader"
          }]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].style.css'),
    new webpack.HotModuleReplacementPlugin()
  ],
  watchOptions: {
    aggregateTimeout: 500
  }
});