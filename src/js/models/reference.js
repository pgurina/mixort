export default [
    {
        title: "Usi Blum",
        subtitle: 'VP BusinessIntelligence <span class="m-nowrap">and Analytics at AppLift</span>',
        text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laboreet dolore magna aliqua.
        Ut enim ad minim veniam, 
        quis nostrud exercitation ullamco 
        laboris nisi ut aliquip ex ea commodo consequat.`,
        image: "/img/png/people/uzi.jpeg",
        menu: [
            {
                link: "",
                iconClass: "up"
            },
            {
                link: "",
                iconClass: "c"
            }
        ]
    },
    {
        title: "Nick Fox",
        subtitle: 'VP BusinessIntelligence <span class="m-nowrap">and Analytics at AppLift</span>',
        text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laboreet dolore magna aliqua.
        Ut enim ad minim veniam, 
        quis nostrud exercitation ullamco 
        laboris nisi ut aliquip ex ea commodo consequat.`,
        image: "/img/png/people/nick.jpeg",
        menu: [
            {
                link: "",
                iconClass: "up"
            },
            {
                link: "",
                iconClass: "c"
            }
        ]
    }
]