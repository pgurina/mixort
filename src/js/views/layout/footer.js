import React from 'react'
import FormView from '../form/form'
import FormRowView from '../form/form_row'

const FooterView = () => {
    return (
    <footer className="m-footer">
        <div className="m-section">
            <div className="row">
                <div className="col col-12 col-sm-7">
                    <h2 className="m-section__header m-section__header--bordered">
                        Let's create something
                        beautiful <span className="m-color--primary">together</span>
                    </h2>
                </div>
                <div className="col col-12 col-sm-5">
                    <FormView>
                        <FormRowView>
                            <input type="text" className="form-control m-input m-input--inversed" placeholder="Name"/>
                        </FormRowView>
                        <FormRowView>
                            <input type="phone" className="form-control m-input m-input--inversed" placeholder="Phone"/>
                        </FormRowView>
                        <FormRowView error={{message:"Email is not correct"}}>
                            <input type="email" className="form-control m-input m-input--invalid" placeholder="Email"/>
                        </FormRowView>
                        <FormRowView className="m-form__row--tall">
                            <textarea className="form-control m-textarea m-textarea--inversed" placeholder="Hi! Please help me :)" rows="6"></textarea>
                            <FormRowView className="row">
                                <div className="col">
                                    <ul className="m-filelist nav">
                                        <li className="m-fileitem nav-item">
                                            <span className="m-fileitem__name">Brief.doc</span><a href="javascript:;" className="m-fileitem__control"></a>
                                        </li>
                                        <li className="m-fileitem nav-item">
                                            <span className="m-fileitem__name">Brief.doc</span><a href="javascript:;" className="m-fileitem__control"></a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col">
                                    <label className="m-fileinput-imposter">
                                        <span className="m-fileinput-imposter__control"></span>
                                        <input type="file" className="m-fileinput-imposter__input"/>
                                    </label>
                                </div>
                            </FormRowView>
                        </FormRowView>
                        <FormRowView className="m-form__row--short">
                            <label className="m-captcha-imposter">
                                <span className="m-captcha-imposter__box m-captcha-imposter__box--checked"></span><span className="m-captcha-imposter__text">I'm not a robot</span>
                            </label>
                        </FormRowView>
                        <FormRowView className="m-form__row--tall">
                            <button type="submit" href="#" className="btn m-btn m-btn--fullwidth m-btn--inverted">Submit</button>
                        </FormRowView>
                    </FormView>
                </div>
            </div>
            <div className="row">
                <div className="col col-12 col-sm-8">
                    <footer className="m-section__footer">
                        <a href="./index.html" className="m-footer__logo">
                            <img src="/img/png/logo--mono-small.png"/>
                        </a>
                        <div>
                            220004, Minsk city,<br/>Amuratorskaya st. 4b, office 24
                        </div>
                        <div>
                            info@aisnovations.com<br/>+375 29 379 90 08
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </footer>
    )
}

export default FooterView