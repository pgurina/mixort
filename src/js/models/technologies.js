export default {
    parentBricks: [
        {
            title: "BackEnd",
            link: "javascript:;",
            children: [
                { shiftTop: "-53.3%", shiftRight: 0, content: "/img/png/technologies/php--mono.png" },
                { shiftTop: 0, shiftRight: "-53.3%", content: "/img/png/technologies/mongo--mono.png"},
                { shiftBottom: "-53.3%", content: "/img/png/technologies/node--mono.png"},
                { shiftBottom: "-106.6%", shiftRight: "0", content: "/img/png/technologies/mysql--mono.png"},
                { shiftTop: "106.6%", shiftRight: "-53.3%", content: "/img/png/technologies/asp--mono.png"}
            ],
            emptyBricks: [
                { col: 1, row: 1},
                { col: 3, row: 1},
                { col: 3, row: 3},
                { col: 2, row: 4},
                { col: 1, row: 5},
                { col: 3, row: 5}
            ]
        },
        {
            title: "FrontEnd",
            link: "javascript:;",
            children: [
                { shiftTop: "-106.6%", content: "/img/png/technologies/react--mono.png" },
                { shiftTop: "-106.6%", shiftRight: "-53.3%", content: "/img/png/technologies/vuejs--mono.png"},
                { shiftTop: "-53.3%", shiftRight: 0, content: "/img/png/technologies/git--mono.png"},
                { shiftTop: 0, shiftRight: "-53.3%", content: "/img/png/technologies/sass--mono.png"},
                { shiftBottom: "-53.3%", content: "/img/png/technologies/angularjs--mono.png"},
                { shiftBottom: "-53.3%", shiftRight: "-53.3%", content: "/img/png/technologies/html5--mono.png"}
            ],
            emptyBricks: [
                { col: 2, row: 1},
                { col: 1, row: 2},
                { col: 3, row: 2},
                { col: 3, row: 4},
                { col: 2, row: 5}
            ]
        }
    ]
}