import React from 'react'

const PlaceView = ({...props})=>{
    return (
        <div className="m-map__place" style={{top: props.shiftY, left: props.shiftX}}>
            <div className="m-tooltip m-tooltip--with-icon">
                <div className="m-tooltip__body">
                    <div className="m-tooltip__text">
                        <span className="m-tooltip__title">{props.title}</span>
                        <span className="m-tooltip__summary">{props.summary}</span>
                        <a href={props.link} className="m-tooltip__link">{props.link_text}</a>
                    </div>
                    <div className="m-tooltip__figure">
                        <span className="m-tooltip__icon">
                            <img src={props.figure}/>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PlaceView