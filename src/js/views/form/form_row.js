import React from 'react'
import InputErrorView from "./input_error"

const FormRowView = ({...props}) => {
    const errorMessage = !!props.error ? <InputErrorView>{props.error.message}</InputErrorView> : ""
    return (
        <div className={`m-form__row ${!!props.className ? props.className : ''}`}>
            {props.children}
            {errorMessage}
        </div>
    )
}

export default FormRowView