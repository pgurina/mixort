import React from 'react'

import navigation from '../../../models/navigation'

import NavBurgerView from './nav_burger'
import ListHorizontalView from '../../lists/list_horizontal'
import ListItemUppercasedView from '../../lists/item_uppercased'

const NavTopView = ({...props}) => {
    const navItems = navigation.map( item => (
        <ListItemUppercasedView key={item.id} itemID={item.id} activeItemID={props.activeItemID}>{item.text}</ListItemUppercasedView>
    ))
    return <nav className={`m-header__nav ${props.className}`} style={props.style}>
        <div className="m-section m-section--no-padding m-header__nav-body">
            <a href="./index.html" className="m-header__logo m-header__logo--small">
                <img src="/img/png/logo--small.png"/>
            </a>
            <ListHorizontalView>
                {navItems}
            </ListHorizontalView>
            <div className="m-header__nav-body">
                <a href="#" className="btn m-btn">Request a quote</a>
                <NavBurgerView navItems={navigation} />
            </div>
        </div>
    </nav>
}

export default NavTopView