export default [
    {
        id: "services",
        text: "Services"
    },
    {
        id: "expertise",
        text: "Expertise"
    },
    {
        id: "technologies",
        text: "Technologies"
    },
    {
        id: "work",
        text: "Work"
    },
    {
        id: "company",
        text: "Company"
    }
]