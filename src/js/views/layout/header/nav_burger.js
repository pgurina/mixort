import React from 'react'

import ListItemView from '../../lists/item'

export default class NavBurgerView extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            popoverVisible: props.popoverVisible || false,
            navItems: props.navItems || [],
            activeItemID: props.activeItemID || ''
        }
        this.toggleMenu = this.toggleMenu.bind(this)
    }

    toggleMenu(evt){
        evt.stopPropagation();
        evt.preventDefault();
        this.setState({
            popoverVisible: !this.state.popoverVisible
        })
    }

    render(){
        const navItems = this.state.navItems.map( (item, index) => (
            <ListItemView key={item.id} itemID={item.id} activeItemID={this.state.activeItemID}>{item.text}</ListItemView>
        ))
        return (
            <button className="btn m-btn--burger" onClick={this.toggleMenu}>
                <nav className="m-popover" style={{visibility: `${this.state.popoverVisible ? 'visible' : 'hidden'}`}}>
                    <header className="m-popover__header">
                        <span className="btn m-popover__close-btn">
                            <span className="m-icon m-icon--close-small"></span>
                        </span>
                    </header>
                    <ul className="m-list">
                        {navItems}
                    </ul>
                </nav>
            </button>
        )
    }
}