import React from 'react'

const BadgeView = ({...props}) => {
    return (
        <div className="m-badge">
            <header className="m-badge__header">
                <figure className="m-badge__icon m-icon m-icon--orbit">
                    <span className={`m-icon m-icon--${props.iconClass}`}></span>
                </figure>
                <h3 className="m-badge__title" dangerouslySetInnerHTML={{__html:props.title}}/>
            </header>
            <div className="m-badge__subtitle">
                {props.children}
            </div>
        </div>
    )
}

export default BadgeView