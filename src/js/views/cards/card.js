import React from 'react'

import CardMenuView from './menu/menu'
import CardMenuItemView from './menu/item'

const CardView = ({...props}) => {
    const cardMenu = props.menu.map( (item,index) => (
        <CardMenuItemView key={index} link={item.link} iconClass={item.iconClass} />
    ))
    return <div className="m-card">
        <figure className="m-card__figure" style={{backgroundImage: `url(${props.image})`}}></figure>
        <section className="m-card__text">
            <header className="m-card__header">
                <h3 className="m-card__title">{props.title}</h3>
                <div className="m-card__subtitle" dangerouslySetInnerHTML={{__html: props.subtitle}}></div>
                <CardMenuView>
                    {cardMenu}
                </CardMenuView>
            </header>
            <section className="m-card__body">{props.children}</section>
        </section>
    </div>
}

export default CardView