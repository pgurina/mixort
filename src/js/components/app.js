import React from 'react'

import advantages from '../models/advantage'
import client from '../models/client'
import experience from '../models/experience'
import people from '../models/person'
import reference from '../models/reference'
import services from '../models/service'
import technologies from '../models/technologies'
import works from '../models/work'

import BadgeView from '../views/badges/badge'
import BannerView from '../views/banners/banner'
import Brick from '../views/masonry/brick'
import CardPersonalView from '../views/cards/card_personal'
import CardView from '../views/cards/card'
import ColDottedView from '../views/cols/col_dotted'
import ColFlippingView from '../views/cols/col_flipping'
import FooterView from '../views/layout/footer'
import HeaderView from '../views/layout/header/header'
import ListItemTallView from '../views/lists/item_tall'
import ListNumberedView from '../views/lists/list_numbered'
import MapView from '../views/maps/map'
import MasonryView from '../views/masonry/masonry'
import MasonryWellView from '../views/masonry/masonry_well'
import SectionView from '../views/layout/section'
import SlideshowView from '../views/slideshow/slideshow'

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            topNavPosition: 0,
            itemsPerSlide: this.calculateItemsPerSlideForSlideshows()
        }
    }

    calculateTopNavPosition(){
        const headerHeight = window.innerHeight
        const scrolled = window.pageYOffset || document.documentElement.scrollTop
        const topNavPosition = scrolled-headerHeight
        this.setState( (state, props) => {
            return { topNavPosition: topNavPosition<0 ? topNavPosition : 0 };
        } )
    }

    calculateItemsPerSlideForSlideshows(){
        const vw = window.innerWidth
        if(vw>=1200){
            return {
                services: 4,
                experiences: 4,
                references: 2
            }
        }else if(vw>=991){
            return {
                services: 3,
                experiences: 3,
                references: 1
            }
        }else if(vw>=768){
            return {
                services: 3,
                experiences: 3,
                references: 1
            }
        }else if(vw>=576){
            return {
                services: 2,
                experiences: 2,
                references: 1
            }
        }else{
            return {
                services: 1,
                experiences: 1,
                references: 1
            }
        }
    }

    componentDidMount(){
        this.calculateTopNavPosition()
        window.onscroll = ()=>{
            this.calculateTopNavPosition()
        }
    }

    padNumber (num) {
        var s = num+"";
        while (s.length < 2) s = "0" + s;
        return s;
    }

    render(){

        const advantagesSection = advantages.map( item => (
            <div key={item.title} className="col col-sm-12 col-md-4 col-12">
                <BadgeView title={item.title} iconClass={item.iconClass}>
                    {item.text}
                </BadgeView>
            </div>
        ) )

        const clientsSection = client.map( (item, index) => (
            <div key={index} className="col col-12 col-sm">
                <Brick className="m-brick--flipping" content={item.content} overlayContent={item.overlayContent} />
            </div>
            
        ))

        const experienceSection = experience.map( (item,index) => {
            const listItems = item.list.map ( (listItem, listIndex) => (
                <ListItemTallView key={listIndex}>{listItem}</ListItemTallView>
            ))
            return (
                <div key={index} className="col col-sm-6 col-md-4 col-lg-4 col-xl-3 col-12 m-slide__child--experiences">
                    <ColFlippingView title={item.title} link={item.link}>
                        <ListNumberedView>
                            {listItems}
                        </ListNumberedView>
                    </ColFlippingView>
                </div>
            )
        })        

        const peopleSection = people.map( person => (
            <div key={person.name} className="col col-lg-3 col-sm-6 col-12">
                <CardPersonalView title={person.name} subtitle={person.job} imageMono={person.imageMono} imageColor={person.imageColor} figureStyle={person.figureStyle} />
            </div>
        ))

        const referenceSection = reference.map( (item, index) => (
            <div key={index} className="col col-lg-6 col-12 m-slide__child--references">
                <CardView {...item}>{item.text}</CardView>
            </div>
        ))

        const servicesSection = services.map( item =>{
            const listItems = item.list.map( (listItem, index) => (
                <li key={`${item.title}-${index}`} className="m-list__item m-list__item--tall">{listItem}</li>
            ) )
            return <div key={item.title} className="col col-sm-6 col-md-4 col-lg-4 col-xl-3 col-12 m-nopadding m-slide__child--services">
                <ColDottedView link="" title={item.title} subtitle={item.subtitle}>
                    <ul className="m-list m-list--numbered m-col__text">
                        {listItems}
                    </ul>
                </ColDottedView>
            </div>
        })

        let counter = 0

        return <div>
            <HeaderView topNavPosition={this.state.topNavPosition} />
            <SectionView number={ this.padNumber(++counter)} title="Services we provide">
                <SlideshowView baseWidthElement="m-slide__child--services" className="m-nomargin" itemsPerSlide={this.state.itemsPerSlide.services}>
                    {servicesSection}
                </SlideshowView>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="Our clients">
                <div className="row">
                    {clientsSection}
                </div>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="Our works">
                <SlideshowView baseWidthElement="m-slide__child--works">
                    <div className="col col-12 m-slide__child--works">
                        <MasonryView items={works}/>
                    </div>
                    <div className="col col-12 m-slide__child--works">
                        <MasonryView items={works}/>
                    </div>
                    <div className="col col-12 m-slide__child--works">
                        <MasonryView items={works}/>
                    </div>
                </SlideshowView>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="References from <span class='m-color--primary'>UpWork</span>">
                <SlideshowView baseWidthElement="m-slide__child--references" itemsPerSlide={this.state.itemsPerSlide.references}>
                    {referenceSection}
                </SlideshowView>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="Our experience">
                <SlideshowView baseWidthElement="m-slide__child--experiences" itemsPerSlide={this.state.itemsPerSlide.experiences}>
                    {experienceSection}
                </SlideshowView>
            </SectionView>
            <BannerView />
            <SectionView number={ this.padNumber(++counter)} title="Technologies we use">
                <MasonryWellView bricks={technologies}/>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="Our advantages">
                <div className="row m-badge-row">
                    {advantagesSection}
                </div>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="Worldwide company">
                <MapView/>
            </SectionView>
            <SectionView number={ this.padNumber(++counter)} title="Company and team">
                <div className="row">
                    {peopleSection}
                </div>
            </SectionView>
            <FooterView/>
        </div>
    }
}