import React from 'react'

import BadgeView from '../../badges/badge'
import NavAsideView from './nav_aside'
import NavTopView from './nav_top'
import ListHorizontalView from '../../lists/list_horizontal'
import ItemUppercasedView from '../../lists/item_uppercased'

const HeaderView = ({...props}) => {
    return <header id="m-site-header" className="m-header">
        <NavTopView activeItemID={props.activeItemID} style={{top: props.topNavPosition+"px"}} className="m-header__nav--fixed"/>
        <NavTopView activeItemID={props.activeItemID} className="m-header__nav--static"/>
        <section className="m-section">
            <figure className="m-header__figure">
                <img src="img/png/background-circle.png"/>
            </figure>
            <section className="m-row m-row--space-between m-header__logo-container">
                <a href="./index.html" className="m-header__logo">
                    <img src="/img/png/logo--big.png"/>
                </a>
                <a href="#" className="btn m-btn m-btn--inverted">Request a quote</a>
            </section>
            <section className="m-row m-header__row--big">
                <NavAsideView activeItemID={props.activeItemID}/>
                <div className="m-header__col m-header__col--large" style={{position:'relative'}}>
                    <h1 className="m-header__title">For the<br/>beautiful <span className="m-header__title-accent">web</span></h1>
                </div>
            </section>
            <section className="m-row m-header__row--medium">
                <div className="m-header__col m-header__col--small">
                    <figure className="m-icon m-icon--arrow-down"></figure>
                    <div className="m-section__body"><b>Scroll</b> Down</div>
                </div>
                <div className="m-header__col m-header__col--large">
                    <h3 className="m-header__subtitle">
                        We bring your web projects to life<br/>
                        From wireframes to full-scale launch
                    </h3>
                </div>
                <div className="m-header__col m-header__language-picker">
                    <ListHorizontalView>
                        <ItemUppercasedView itemID="pl">PL</ItemUppercasedView>
                        <ItemUppercasedView activeItemID="en" itemID="en">EN</ItemUppercasedView>
                        <ItemUppercasedView itemID="ru">RU</ItemUppercasedView>
                    </ListHorizontalView>
                </div>
            </section>
            <section className="m-row">
                <div className="row m-badge-row">
                    <div className="col col-12 col-md-4">
                        <BadgeView title="Dedicated<br/>team" iconClass="team">
                            Deliver projects <span className="m-nowrap">on-time</span> <span className="m-nowrap">and on-budget</span> with a dedicated team of skilled and experienced developers
                        </BadgeView>
                    </div>
                    <div className="col col-12 col-md-4">
                        <BadgeView title="Develop competitive" iconClass="develop">
                            We can rapidly develop a fully functional web or mobile app, so you can showcase your idea to evangelists and investors
                        </BadgeView>
                    </div>
                    <div className="col col-12 col-md-4">
                        <BadgeView title="Maintenance and&nbsp;Support" iconClass="maintenance">
                            We will ensure availability and reliability of all your online resorces. We will keep your business online 24/7
                        </BadgeView>
                    </div>
                </div>
            </section>
        </section>
    </header>
}

export default HeaderView