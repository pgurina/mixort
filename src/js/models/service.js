export default [
    {
        title: "Back-end Development",
        subtitle: "Rocket fast backend with latest technologies",
        link: "",
        list: [ "NodeJS", "MongoDB", "Docker"]
    },
    {
        title: "Front-end Development",
        subtitle: "Engaging interaction with state of the art frameworks",
        link: "",
        list: [ "VueJS", "React", "HTML5"]
    },
    {
        title: "Design",
        subtitle: "Beautiful web design with illustrations and animation",
        link: "",
        list: [ "Web Design", "Illustrations", "UI/UX"]
    },
    {
        title: "Quality assurance",
        subtitle: "Rigorous manual and automated testing",
        link: "",
        list: [ "Functional testing", "Performance testing", "Maintenance"]
    }
]