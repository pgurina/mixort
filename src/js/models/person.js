export default [
    {
        name: "Sergey Galuza",
        job: "Founder",
        imageMono: "/img/png/people/galuza-mono.png",
        imageColor: "/img/png/people/galuza.png"
    },
    {
        name: "Vitaly Stupak",
        job: "Director",
        imageMono: "/img/png/people/stupak-mono.png",
        imageColor: "/img/png/people/stupak.png"
    },
    {
        name: "Alexander Schebet",
        job: "CTO",
        imageMono: "/img/png/people/shebet-mono.png",
        imageColor: "/img/png/people/shebet.png",
        figureStyle: {left: "-12%"}
    },
    {
        name: "Vitaly Yurkevich",
        job: "Customer relations<br/>manager",
        imageMono: "/img/png/people/yurkevich-mono.png",
        imageColor: "/img/png/people/yurkevich.png"
    }
]