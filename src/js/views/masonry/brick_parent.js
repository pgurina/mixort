import React from 'react'

const BrickParentView = ({...props}) => (
    <a href={props.link} className="m-brick m-brick--parent" key={props.key}>
        <div className="m-brick__content">
            <h3 className="m-brick__title m-color--primary">{props.title}</h3>
        </div>
        {props.children}
    </a>
)

export default BrickParentView