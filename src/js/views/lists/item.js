import React from 'react'

const ListItemView = ({...props}) => {
    const stopPropagation = (evt)=>{
        evt.stopPropagation()
        return true;
    }
    return (
        <li className="m-list__item">
            <a onClick={stopPropagation} href={props.itemID} className={`m-list__link ${props.activeItemID===props.itemID ? 'active' : ''}`}>{props.children}</a>
        </li>
    )
}

export default ListItemView