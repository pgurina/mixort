import React from 'react'

const CardPersonalView = ({...props}) => {
    return (
        <a href={!!props.link ? props.link : "javascript:;"} className="m-card m-card--personal">
            <header className="m-card__header">
                <h3 className="m-card__title m-card__title--small">{props.title}</h3>
                <div className="m-card__subtitle m-card__subtitle--small" dangerouslySetInnerHTML={{__html: props.subtitle}}></div>
            </header>
            <div className="m-card__overlay">
                <figure className="m-card__figure m-card__figure--inset" style={!!props.figureStyle ? props.figureStyle : {}}>
                    <img src={props.imageMono}/>
                </figure>
            </div>
            <div className="m-card__overlay m-card__overlay--hideable">
                <figure className="m-card__figure m-card__figure--inset" style={!!props.figureStyle ? props.figureStyle : {}}>
                    <img src={props.imageColor}/>
                </figure>
            </div>
        </a>
    )
}

export default CardPersonalView