import React from 'react'

const BrickDottedView = ({...props}) => (
    <div key={props.key} className="m-brick m-brick--dotted" style={{gridRowStart: props.row, gridColumnStart: props.col}}></div>
)

export default BrickDottedView