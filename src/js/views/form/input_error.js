import React from 'react'

const InputErrorView = ({...props}) => <div className="m-input__error">{props.children}</div>

export default InputErrorView