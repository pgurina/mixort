import React from 'react'

const ColDottedView = ({...props}) => {
    return (
        <a href={props.link} className="m-col m-col--dotted m-col--bordered">
            <div className="m-col__body">
                <header className="m-col__header">
                    <h3 className="m-col__title">{props.title}</h3>
                </header>
                <div className="m-col__subtitle">{props.subtitle}</div>
                <div className="m-col__text">{props.children}</div>
            </div>
        </a>
    )
}

export default ColDottedView