import React from 'react'

const BannerView = ({...props})=>{
    return (
        <section className="m-banner">
            <figure className="m-banner__figure">
                <img src="/img/png/background-circle-quarter.png"/>
            </figure>
            <img src="/img/png/background-circle-photo.png" className="m-banner__image"/>
            <section className="m-section">
                <h2 className="m-section__header m-section__header--inverted m-section__header--bordered m-banner__title">
                    Leave you email address to schedule an individual free consultation
                </h2>
                <form className="m-banner__form">
                    <div className="row">
                        <div className="col">
                            <input type="email" className="form-control m-input m-input--accent" placeholder="E-mail" />
                        </div>
                        <div className="col">
                            <button type="submit" href="#" className="btn m-btn m-btn--inverted">Find solution</button>
                        </div>
                    </div>
                </form>
            </section>
        </section>
    )
}

export default BannerView