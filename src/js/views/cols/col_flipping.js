import React from 'react'

const ColFlippingView = ({...props}) => {
    return (
        <div className="m-col m-col--flipping m-col--bordered m-col--tall">
            <header className="m-col__header">
                <h3 className="m-col__title" dangerouslySetInnerHTML={{__html: props.title}}></h3>
            </header>
            {props.children}
            <div className="m-col__footer">
                <div className="m-col__content"><a href={props.link} className="btn m-btn m-btn--nobg">View cases</a>
                </div>
                <div className="m-col__overlay">
                    <a href={props.link} className="btn m-btn m-btn--inverted">View cases</a>
                </div>
            </div>
        </div>
    )
}

export default ColFlippingView