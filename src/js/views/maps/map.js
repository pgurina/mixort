import React from 'react'
import PlaceView from './place'

const MapView = ({...props})=>{
    const handleSVGError = (el) => {
        return ()=>{
            el.onError=null;
            el.src='img/png/map.png'
        }
    }
    return (
        <div className="m-map">
            <div className="m-map__body">
                <figure className="m-map__bg">
                    <img src="img/svg/map.svg" onError={handleSVGError(this)}/>
                </figure>
                <div className="m-map__overlay">
                    <PlaceView 
                        shiftX="57.5%"
                        shiftY="30.8%"
                        title="Meitan"
                        summary="Russia, Barnaul"
                        link=""
                        link_text="meitan.ru"
                        figure="/img/png/logo--mono-small.png"/>
                </div>
            </div>
        </div>
    )
}

export default MapView