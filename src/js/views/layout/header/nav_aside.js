import React from 'react'

import navigation from '../../../models/navigation'

import ListView from '../../lists/list'
import ListItemUppercasedView from '../../lists/item_uppercased'

const NavAsideView = ({...props}) => {
    const navItems = navigation.map( item => (
        <ListItemUppercasedView key={item.id} itemID={item.id} activeItemID={props.activeItemID}>{item.text}</ListItemUppercasedView>
    ))
    return <nav className="m-header__nav--aside m-header__col m-header__col--small">
        <ListView>
            {navItems}
        </ListView>
    </nav>
}

export default NavAsideView