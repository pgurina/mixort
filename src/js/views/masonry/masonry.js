import React from 'react'

import BrickView from './brick'

const MasonryView = ({...props}) => {
    const bricks = props.items.map( (item,index) => (
        <BrickView key={index} image={item.image} overlayContent={item.overlayContent} className={item.className} />
    ))
    return <div className="m-masonry">
        {bricks}
    </div>
}

export default MasonryView