import React from 'react'

const SectionView = ({...props}) => {
    return (
        <section className="m-section">
            <h2 className="m-section__header"><span className="m-section__number">{props.number}.</span><span dangerouslySetInnerHTML={{__html: props.title}} /></h2>
            <section className="m-section__body">
                {props.children}
            </section>
        </section>
    )
}

export default SectionView