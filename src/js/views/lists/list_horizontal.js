import React from 'react'

const ListHorizontalView = ({...props}) => (
    <ul className="m-list m-list--horizontal nav">{props.children}</ul>
)

export default ListHorizontalView