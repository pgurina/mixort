import React from 'react'

const ListHorizontalView = ({...props}) => (
    <ul className="m-list">{props.children}</ul>
)

export default ListHorizontalView