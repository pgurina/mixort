import React from 'react'

import SlideView from './slide'

export default class SlideshowView extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            slides: [],
            currentSlide: 0,
            shiftLeft: 0,
            leftControlVisible: false,
            rightControlVisible: true,
            indicatorProgress: 0,
            itemsPerSlide: this.props.itemsPerSlide || 1
        }
        this.moveSlidesLeft = this.moveSlidesLeft.bind(this)
        this.moveSlidesRight = this.moveSlidesRight.bind(this)
    }

    componentDidMount(){
        //Determine width of each slide and indicator progress by separating children to different slides and measuring their width per slide
        this.setState( (state,props) => {
            let slides = this.separateContentIntoSlides(this.props.children).map( (slide, index) => {
                const slideChildren = document.getElementsByClassName(this.props.baseWidthElement)
                const childWidth = !!slideChildren[0] ? slideChildren[0].offsetWidth : document.documentElement.offsetWidth
                return {
                    slide: slide,
                    slideWidth: slide.length * childWidth
                }
            } )
            return {slides: slides, indicatorProgress: this.updateIndicatorProgress(state.currentSlide+1, slides)}
        })
    }

    moveSlidesLeft(){
        const currentSlide = this.state.currentSlide
        const nextSlide = currentSlide+1
        if(nextSlide>=this.state.slides.length) return
        const nextSlideWidth = this.state.slides[nextSlide].slideWidth
        const progress = this.updateIndicatorProgress(nextSlide+1, this.state.slides)
        this.setState( (state,props) => {
            return {
                currentSlide: nextSlide,
                shiftLeft: state.shiftLeft - nextSlideWidth,
                leftControlVisible: progress>1/this.state.slides.length,
                rightControlVisible: progress<1,
                indicatorProgress: progress
            }
        })
    }

    moveSlidesRight(){
        const currentSlide = this.state.currentSlide
        const nextSlide = currentSlide-1
        if(nextSlide<0) return
        const currentSlideWidth = this.state.slides[currentSlide].slideWidth
        const progress = this.updateIndicatorProgress(nextSlide+1, this.state.slides)
        this.setState( (state,props) => {
            return {
                currentSlide: nextSlide,
                shiftLeft: state.shiftLeft + currentSlideWidth,
                leftControlVisible: progress>1/this.state.slides.length,
                rightControlVisible: progress<1,
                indicatorProgress: progress
            }
        })
    }

    separateContentIntoSlides(content){
        const children = React.Children.toArray(content)
        let slides = []

        while(children.length>0){
            slides.push(children.splice(0, this.state.itemsPerSlide))
        }
        return slides
    }

    updateIndicatorProgress(nextSlide, slides){
        return nextSlide/slides.length
    }

    render(){
        return (
            <div className="m-slideshow">
                <section className="m-slideshow__body">
                    <div className={`m-slideshow__track row ${this.props.className}`} style={{transform: `translateX(${this.state.shiftLeft}px)`}}>
                        {this.props.children}
                    </div>
                </section>
                <section className={`m-slideshow__footer ${this.state.slides.length<=1 ? 'm-hidden' : ''}`}>
                    <div className="m-slideshow__indicator">
                        <div className="m-slideshow__indicator-progress" style={{transform: `translateX(${this.state.indicatorProgress*100}%)`}}></div>
                    </div>
                    <div className="m-slideshow__controls">
                        <a href="javascript:;" className={`btn m-btn m-btn--round m-control m-control--left ${!this.state.leftControlVisible ? 'm-hidden' : ''}`} onClick={this.moveSlidesRight}></a>
                        <a href="javascript:;" className={`btn m-btn m-btn--round m-control m-control--right  ${!this.state.rightControlVisible ? 'm-hidden' : ''}`}onClick={this.moveSlidesLeft}></a>
                    </div>
                </section> 
            </div>
        )
    }
}