import React from 'react'

const BrickChildView = ({...props}) => (
    <div key={props.key} className="m-brick m-brick--child" style={{
            top: props.shiftTop,
            right: props.shiftRight,
            bottom: props.shiftBottom,
            left: props.shiftLeft
        }}>
        <div className="m-brick__content">
            <div className="m-brick__text">
                <img src={props.children}/>
            </div>
        </div>
    </div>
)

export default BrickChildView