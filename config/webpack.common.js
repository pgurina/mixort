const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

let cleanOptions = {
  root:     path.resolve('../'),
  verbose:  true,
  dry:      false
}

module.exports = {
  entry: {
    main: './src/js/index.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve('app/dist')
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([path.resolve('app/dist')], cleanOptions)
  ]
};