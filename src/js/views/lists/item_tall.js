import React from 'react'

const ListItemTallView = ({...props}) => (
    <li className="m-list__item m-list__item--tall" dangerouslySetInnerHTML={{__html: props.children}}></li>  
)

export default ListItemTallView