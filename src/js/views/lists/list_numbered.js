import React from 'react'

const ListNumberedView = ({...props}) => (
    <ul className="m-list m-list--numbered">{props.children}</ul>
)

export default ListNumberedView