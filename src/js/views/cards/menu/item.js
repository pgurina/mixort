import React from 'react'

const CardMenuItemView = ({...props}) => (
    <li className="nav-item">
        <a className="nav-link m-list__link" href={props.link}>
            <span className={`m-company-icon m-company-icon--${props.iconClass}`}></span>
        </a>
    </li>
)

export default CardMenuItemView