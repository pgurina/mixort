import React from 'react'

import BrickChildView from '../masonry/brick_child'
import BrickDottedView from '../masonry/brick_dotted'
import BrickParentView from '../masonry/brick_parent'

const MasonryWellView = ({...props}) => {

    const parentBricks = props.bricks.parentBricks.map( (brick,index) => {
        const childBricks = brick.children.map( (childBrick) => (
            <BrickChildView 
                key={childBrick.content} 
                shiftLeft={childBrick.shiftLeft}
                shiftRight={childBrick.shiftRight}
                shiftTop={childBrick.shiftTop}
                shiftBottom={childBrick.shiftBottom}>
                {childBrick.content}
            </BrickChildView>
        ))
        const emptyBricks = brick.emptyBricks.map( (brick) => (
            <BrickDottedView row={brick.row} col={brick.col} key={`brick-${brick.row}-${brick.col}`} />
        ) )
        return (
            <div className="col col-12 col-sm" key={index}>
                <div className="m-masonry m-masonry--well">
                    {emptyBricks}
                    <BrickParentView title={brick.title} link={brick.link}>
                        {childBricks}
                    </BrickParentView>
                </div>
            </div>
        )
    })

    return (
        <div className="row">
            {parentBricks}
        </div>
    )
}
export default MasonryWellView